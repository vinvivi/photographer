function showPhoto () {
    let arrPhoto = [
        '1.jpg',
        '2.jpg',
        '3.jpg',
        '4.jpg',
        '5.jpg',
        '6.jpg',
        '7.jpg',
        '8.jpg',
        '9.jpg',
        '10.jpg',
        '11.jpg',
        '12.jpg',
        '13.jpg',
        '14.jpg',
        '15.jpg',
        '16.jpg',
        '17.jpg',
        '18.jpg',
        '19.jpg',
        '20.jpg',
        '21.jpg',
        '22.jpg',
        '23.jpg',
        '24.jpg',
        '25.jpg',
        '26.jpg',
        '27.jpg',
        '28.jpg',
        '29.jpg',
        '30.jpg',
        '31.jpg',
        '32.jpg',
        '33.jpg',
        '34.jpg',
        '35.jpg',
    ];
    let step;
    let galleryContainer = document.getElementById('gallery');
    let style = document.createElement('style');
    for (step = 0; step < 7; step++) {
        const randomPhoto = arrPhoto[Math.floor(Math.random() * arrPhoto.length)];
        console.log("random arrPhoto =>", randomPhoto);
        style.innerHTML += ".gallery-image-" + step + "{background-image: url(gallery_images/" + randomPhoto + ");}\n\r";
    }

    if ( galleryContainer.children[0].nodeName === 'STYLE' ) {
        galleryContainer.children[0].remove();
    }
    galleryContainer.prepend(style);
}
showPhoto();

