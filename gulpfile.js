/* подключаем нужные пакеты*/
const gulp = require('gulp'); /*подкл gulp*/
const gulpEjsMonster = require('gulp-ejs-monster');
const rename = require('gulp-rename'); /*переименование файлов*/
const flatten = require('gulp-flatten'); /*возвращает компилированые файлы без папок и вложений*/
const concat = require('gulp-concat-css');/*соединяет файлы css в один*/
const less = require('gulp-less');/*компилирует less в css*/
const lessImport = require('gulp-less-import');/*компилирует less в css*/
const imagemin = require('gulp-imagemin'); /*ужимает изображения*/
const replace = require('gulp-replace'); /*ужимает изображения*/

/**
 * обьявляем таски
 *
 */
/*собираем шаблон html и возвращаем в папку public */
const views = function() {
    return gulp.src(['src/views/pages/**/*.ejs', '!src/views/pages/**/sections/**/*.*']) /*собираем header, layout, footer в один файл отправляем в  папку pages */
        .pipe(gulpEjsMonster({/* plugin options */}))
        .pipe(rename({extname: '.html'})) /*меняем тип файла с ejs на html*/
        .pipe(flatten()) /*возвращаем файл без вложений и папок*/
        .pipe(gulp.dest('./public')); /*возвращаем готовую страницу html в папку public*/
};
/**
 *
 * собираем стили
 */
const styles = function() {
    return gulp.src(['src/assets/styles/variables.less','src/views/**/*.less'])/*собираем все файлы less*/
        .pipe(lessImport('src/assets/styles/main.less'))/* импортируем в main.less */
        .pipe(less({relativeUrls: true, rootpath: 'public'})) /* компилируем в css */
        .pipe(concat('style.css')) /* rename to style.css */
        .pipe(replace(/url\(\".*\//g, 'url("images/'))/*меняем путь картинок  в готовом сss*/
        .pipe(gulp.dest('./public/')); /* put into /public folder */
};
/**
 *
 * компилируем изображения
 */
const images = function() {
    return gulp.src(['src/views/**/*.{png,jpg,bmp}', '!src/views/pages/index/sections/gallery/gallery_images/**']) /*собираем все файлы с расширениеми png,jpg,bmp */
        // .pipe(imagemin([ /*обрабатываем изображения*/
        //     imagemin.mozjpeg({quality: 75, progressive: true}),
        //     imagemin.optipng({optimizationLevel: 5}),
        // ]))
        .pipe(flatten()) /*возвращаем файлы массивом без вложений и папок*/
        .pipe(gulp.dest('./public/images')); /*возвращаем обработаные изображения в папку public */;
};

const images_gallery = function() {
    let index = 0;
    return gulp.src(['src/views/pages/index/sections/gallery/gallery_images/**/*.{png,jpg,bmp}']) /*собираем все файлы с для галереи*/
        .pipe(rename(function (path) {
            index++;
            return {
                dirname: path.dirname,
                basename: index,
                extname: path.extname
            }; /* переименовываем файлы по порядку*/
        }))
        .pipe(imagemin([ /*обрабатываем изображения*/
            imagemin.mozjpeg({quality: 75, progressive: true}),
            imagemin.optipng({optimizationLevel: 5}),
        ]))
        .pipe(flatten()) /*возвращаем файлы массивом без вложений и папок*/
        .pipe(gulp.dest('./public/gallery_images')); /*возвращаем обработаные изображения в папку public */;
};

const js = function() {
    return gulp.src('src/views/pages/**/*.js') /*собираем js, layout, отправляем в  папку pages */
        .pipe(flatten()) /*возвращаем файл без вложений и папок*/
        .pipe(gulp.dest('./public')); /*возвращаем в папку public*/
};


const watch = function() {
    gulp.watch('src/**/*', gulp.parallel(views, styles, images, js));/*watcher for changes*/
};
/**
 *
 * выполняем таски
 */
exports.views = views;
exports.styles = styles;
exports.images = images;
exports.watch = watch;
exports.images_gallery = images_gallery;
exports.js = js;
exports.default = gulp.parallel(views, styles, images, images_gallery, js); /*выполняем задачи асинхронно*/